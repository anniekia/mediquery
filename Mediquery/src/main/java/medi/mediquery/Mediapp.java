package medi.mediquery;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Mediapp extends Application {
    private static Stage pStage;
    @Override
    public void start(Stage stage) throws IOException {
        //NEED THIS! DO NOT TOUCH
        pStage = stage;
        //NEED THIS! DO NOT TOUCH
        FXMLLoader fxmlLoader = new FXMLLoader(Mediapp.class.getResource("medi-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Mediquery");
        stage.setScene(scene);
        stage.show();
    }

    public static Stage getpStage() {
        return pStage;
    }

    public static void main(String[] args) {
        launch();
    }
}