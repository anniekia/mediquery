package medi.mediquery;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class PrescriptionEditor {

    LoginView a = new LoginView();

    @FXML
    private Button AddButton;

    @FXML
    private TextField dayLabel;

    @FXML
    private TextField leftLabel;

    @FXML
    private TextField nameLabel;

    @FXML
    private TextField timeLabel;

    @FXML
    void AddPrescript(ActionEvent event) {
        try{
            MySQL_connect.change_medication_plan(nameLabel.getText(),
                    LoginView.accountName,
                    timeLabel.getText(),
                    dayLabel.getText(),
                    Integer.parseInt(leftLabel.getText()));
            Stage stage = (Stage) AddButton.getScene().getWindow();

            stage.close();
        } catch (Exception e){
            e.printStackTrace();
        }

    }

}
