package medi.mediquery;

import java.sql.*;

public class MySQL_connect {
    static Connection con;
    static Statement st;


    /*sql table names
        prescription: medication_plan
        drug: drug_list
     */

    public static void connection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("Driver loaded successfully");
            con = DriverManager.getConnection("jdbc:mysql://199.244.49.188:3306/mediquery_data", "Haofan", "1052");
            System.out.println("Successful connection");
            st = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            System.out.println("Statement created successfully");
            System.out.println("Now, You can access the database");
            System.out.println("==========================================================================");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void test_account_list() throws SQLException {
       /* String sql;
        String sql2;
        sql = "SELECT * FROM mediquery_data.prescrition;";
        sql2 = "SELECT * FROM mediquery_data.account_list;";
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            String drug_name = rs.getString("drug_name");
            String use_times = rs.getString("use_times");
            String days_take = rs.getString("days_take");
            int days_left = rs.getInt("days_left");
            System.out.print("Drug: " + drug_name);
            System.out.print(", use: " + use_times);
            System.out.print(", take: " + days_take);
            System.out.print(", left: " + days_left);
            System.out.print("\n");
        }
        rs = st.executeQuery(sql2);
        while (rs.next()) {
            String user_name = rs.getString("user_name");
            String user_password = rs.getString("user_password");

            System.out.print("user:            " + user_name);
            System.out.print(", password:          " + user_password);
            System.out.print("\n");
        }
        System.out.println(Authentication("userB@gmail.com","asdf4321"));
        System.out.println(Authentication("userA@gmail.com","asdf1234"));
        System.out.println(Authentication("userA@gmai.com","asdf1234"));*/
        String t1_account = "userE@gmai.com";
        String t1_password = "asdf1234";
        String t1_name = "Amy";
        add_user(t1_account, t1_password,t1_name);
        delete_user(t1_account, t1_password);
        change_password("userA@gmail.com", t1_password,"12345678");
        change_password("userA@gmail.com", "12345678",t1_password);
        change_name("userA@gmail.com", t1_password,"anli");
        change_name("userA@gmail.com", t1_password,"Bob");
        System.out.println("=================================test_account_list end=========================================");
    }
    public static void test_drug_list() throws SQLException {
        int t1_drug_id = 11005;
        String t1_drug_name = "Vitamin E";
        String t1_suggested_dose = "15mcg/day";
        String t1_production_company = "aka Inc";
        add_drug(t1_drug_id, t1_drug_name, t1_suggested_dose, t1_production_company);
        System.out.println(check_drug_list(t1_drug_id, t1_drug_name, t1_suggested_dose, t1_production_company));
        delete_drug(t1_drug_id);
        change_drug_data(11001,"rrr","155/adt", "dwawd");
        change_drug_data(11001,"Vitaming A","1400mg/day", "NatureMade Inc");
        System.out.println("=================================test_drug_list end=========================================");
    }
    public static void test_medication_plan() throws SQLException {
        String t1_drug_name = "11005";
        String t1_user_account = "Vitamin E";
        String t1_use_times = "13:00";
        String t1_days_take = "aka Inc";
        int t1_days_left = 40;
        add_medication_plan(t1_drug_name, t1_user_account, t1_use_times, t1_days_take,t1_days_left);
        System.out.println(check_medication_plan(t1_drug_name, t1_user_account));
        delete_medication_plan(t1_drug_name,t1_user_account);
        change_medication_plan("Vitaming A","userA@gmail.com", "12:00", "asasddf",15);
        change_medication_plan("Vitaming A","userA@gmail.com","12:00", "Two pills each time",40);
        System.out.println("=================================test_medication_plan end=========================================");
    }

    //Second Authentication for just checking if email already exists
    public static boolean Authentication(String account){
        try{
            String sql_check = String.format("select * from account_list where user_account = '%s'", account);
            ResultSet ck = st.executeQuery(sql_check);
            if(ck.first()){
                return true;
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static boolean Authentication(String account, String password){
        try{
            String sql_check = String.format("select * from account_list where user_account = '%s' and user_password='%s'", account , password);
            ResultSet ck = st.executeQuery(sql_check);
            if(ck.first()){
                return true;
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static void add_user(String account, String password, String username) throws SQLException {
        try{
            String sql_add;
            if(Authentication(account,password)){System.out.println("account already exist");}
            else{
                sql_add = String.format("INSERT INTO account_list VALUES('%s','%s','%s');",account,password,username);
                st.executeUpdate(sql_add);
                if(Authentication(account,password)) {
                    System.out.println("account add success");}
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void delete_user(String account, String password) throws SQLException {
        try{
            if(Authentication(account,password)) {
                String sql_delete = String.format("DELETE FROM account_list where user_account = '%s'",account);
                st.executeUpdate(sql_delete);
                System.out.println("account delete success");
            }
            else
                System.out.println("account delete error");
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void change_password(String account, String old_password, String new_password) throws SQLException {
        try{
            if(Authentication(account,old_password)) {
                String sql_update = String.format("UPDATE account_list SET user_password = '%s' where user_account = '%s'", new_password, account);
                st.executeUpdate(sql_update);
                System.out.println("password change success");
            }
            else
                System.out.println("password change error");
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void change_name(String account,String password,String new_name) throws SQLException {
        try{
            if(Authentication(account,password)) {
                String sql_update = String.format("UPDATE account_list SET user_name = '%s' where user_account = '%s'", new_name, account);
                st.executeUpdate(sql_update);
                System.out.println("name change success");
            }
            else
                System.out.println("name change error");
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean check_drug_list(int drug_id,  String drug_name, String suggested_dose, String production_company) {
        try {
            String sql_check = String.format("select * from drug_list where drug_id = '%s' and drug_name ='%s' and suggested_dose = '%s' and production_company = '%s'", drug_id, drug_name, suggested_dose, production_company);
            ResultSet ck = st.executeQuery(sql_check);
            if (ck.first()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static boolean check_drug_id(int drug_id) {
        try {
            String sql_check = String.format("select * from drug_list where drug_id = '%s'", drug_id);
            ResultSet ck = st.executeQuery(sql_check);
            if (ck.first()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static void add_drug(int drug_id,  String drug_name, String suggested_dose, String production_company) throws SQLException {
        try{
            String sql_add;
            if(check_drug_id(drug_id)) {
                System.out.println("data already exist");
            }
            else{
                sql_add = String.format("INSERT INTO drug_list VALUES('%s','%s','%s','%s')", drug_id, drug_name, suggested_dose, production_company);
                st.executeUpdate(sql_add);
                System.out.println("data add success");
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void delete_drug(int drug_id) throws SQLException {
        try{
            if(check_drug_id(drug_id)) {
                String sql_delete = String.format("DELETE FROM drug_list where drug_id = '%s'",drug_id);
                st.executeUpdate(sql_delete);
                System.out.println("drug delete success");
            }
            else
                System.out.println("drug delete error");
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void change_drug_data(int drug_id,  String drug_name, String suggested_dose, String production_company) throws SQLException {
        try{
            if(check_drug_id(drug_id)) {
                String sql_update = String.format("UPDATE drug_list SET drug_name = '%s' and suggested_dose = '%s' and suggested_dose = '%s' where production_company = '%s'", drug_id, drug_name, suggested_dose, production_company);
                st.executeUpdate(sql_update);
                System.out.println("drug_data change success");
            }
            else
                System.out.println("drug_data change error");
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean check_medication_plan(String drug_name, String user_account) {
        try {
            String sql_check = String.format("select * from medication_plan where drug_name = '%s' and user_account = '%s'", drug_name, user_account);
            ResultSet ck = st.executeQuery(sql_check);
            if (ck.first()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static void add_medication_plan(String drug_name, String user_account, String use_times, String days_take, int days_left) throws SQLException {
        try{
            String sql_add;
            if(check_medication_plan(drug_name, user_account)) {
                System.out.println("medication plan already exist");
            }
            else{
                sql_add = String.format("INSERT INTO medication_plan VALUES('%s','%s','%s','%s','%s')", drug_name, user_account, use_times, days_take, days_left);
                st.executeUpdate(sql_add);
                System.out.println("medication plan add success");
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void delete_medication_plan(String drug_name, String user_account) throws SQLException {
        try{
            if(check_medication_plan(drug_name,user_account)) {
                String sql_delete = String.format("DELETE FROM medication_plan where drug_name = '%s' and user_account = '%s'",drug_name,user_account);
                st.executeUpdate(sql_delete);
                System.out.println("medication plan delete success");
            }
            else
                System.out.println("medication plan delete error");
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void change_medication_plan(String drug_name, String user_account, String use_times, String days_take, int days_left) throws SQLException {
        try{
            if(check_medication_plan(drug_name,user_account)) {
                String sql_update = String.format("UPDATE medication_plan SET use_times = '%s' , days_take = '%s' , days_left = '%s' where drug_name = '%s' and user_account = '%s'", use_times, days_take,days_left, drug_name, user_account);
                st.executeUpdate(sql_update);
                System.out.println("medication plan change success");
            }
            else
                System.out.println("medication plan change error");
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[]args) throws SQLException {
        connection();
        test_account_list();
        test_drug_list();
        test_medication_plan();
    }
}

