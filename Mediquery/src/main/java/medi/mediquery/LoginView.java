package medi.mediquery;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;


public class LoginView {
    public static String accountName;

    @FXML
    private PasswordField PassText;

    @FXML
    private TextField EmailBox;

    @FXML
    private Label StatusLabel;

    @FXML
    private TextField UserText;

    @FXML
    void register(ActionEvent event) {
        try{
            if(MySQL_connect.Authentication(EmailBox.getText())) {
                StatusLabel.setText("Account Already Exists");

                StatusLabel.setTextFill(Color.color(1, 0, 0));
            }else{
                MySQL_connect.add_user(EmailBox.getText(), PassText.getText(), UserText.getText());
                StatusLabel.setText("Account Created");
                accountName = EmailBox.getText();
                StatusLabel.setTextFill(Color.color(0, 1, 0));
            }

        }catch (Exception e){
            e.printStackTrace();

        }

    }

    @FXML
    void signIn(ActionEvent event) {
        try{
            if(MySQL_connect.Authentication(EmailBox.getText(), PassText.getText())){
                StatusLabel.setText("Sign-In Successful");
                StatusLabel.setTextFill(Color.color(0, 1, 0));
                accountName = EmailBox.getText();
            }else{
                StatusLabel.setText("Email or Password Incorrect");
                StatusLabel.setTextFill(Color.color(1, 0, 0));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public String getAccountName() {
        return accountName;
    }
}
