package medi.mediquery;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static medi.mediquery.MySQL_connect.*;

public class MediController {
    LoginView a = new LoginView();

    @FXML
    private Button NewCollumButton;

    @FXML
    private Button EditPrescription;

    @FXML
    private TableColumn<Prescription, Integer> dayLeftCol;

    @FXML
    private TableColumn<Prescription, String> dayTakeCol;

    @FXML
    private TableView<Prescription> drugTable;

    @FXML
    private TableColumn<Prescription, String> genNameCol;
    @FXML
    private TableColumn<Prescription, String> timeCol;
    @FXML
    public void initialize() throws SQLException {

        dayLeftCol.setCellValueFactory(new PropertyValueFactory<>("daysLeft"));
        genNameCol.setCellValueFactory(new PropertyValueFactory<>("drug"));
        dayTakeCol.setCellValueFactory(new PropertyValueFactory<>("days"));
        timeCol.setCellValueFactory(new PropertyValueFactory<>("time"));
        getData();
        connection();
    }
    @FXML
    void Refresh(ActionEvent event) {
        drugTable.getItems().clear();
        getData();
    }

    //Gathers data from table.
    public void getData() {
        ResultSet rs = null;
        try {
            connection();
            rs = st.executeQuery(String.format("SELECT * FROM medication_plan where user_account = '%s'", a.getAccountName()));
            while(rs.next()){
                //Should populate table, worked for me. Should show 4 entries for Vitaming A-D that lines up with console prompt.
                drugTable.getItems().add(new Prescription((rs.getString("drug_name")),
                        (rs.getString("use_times")),
                        (rs.getString("days_take")),
                        (rs.getInt("days_left"))));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void openLogin(ActionEvent event) {
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("login-view.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(Mediapp.getpStage());
            stage.setTitle("Login");
            stage.setScene(new Scene(root1));
            stage.show();


        } catch (Exception e){
            e.printStackTrace();
            System.out.println("New scene unable to load");
        }



    }

    @FXML
    void editPrescription(ActionEvent event) {
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("edit-view.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(Mediapp.getpStage());
            stage.setTitle("Edit Perscription");
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("New scene unable to load");
        }
    }
    @FXML
    void newPrescription(ActionEvent event) {
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("medi-view2.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(Mediapp.getpStage());
            stage.setTitle("New Perscription");
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("New scene unable to load");
        }
    }

    @FXML
    void openQuestion(ActionEvent event) {
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("mediquery.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(Mediapp.getpStage());
            stage.setTitle("Medical Question");
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("New scene unable to load");
        }

    }

    @FXML
    void drugTake(ActionEvent event) {
        try{
            PreparedStatement updt = con.prepareStatement(String.format("UPDATE medication_plan SET days_left = days_left - 1 WHERE user_account = '%s'", LoginView.accountName));
            updt.executeUpdate();
            drugTable.getItems().clear();
            getData();
        }catch (Exception e){
            e.printStackTrace();
        }

    }


}
