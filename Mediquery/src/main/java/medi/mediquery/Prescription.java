package medi.mediquery;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Prescription {
    private SimpleStringProperty drug;
    private SimpleStringProperty days;
    private SimpleStringProperty time;
    private SimpleIntegerProperty daysLeft;

    //Accepts Strings and converts them into SimpleStringProperties so that it can be put on table.
    //It literally has to be like this or it doesn't function
    public Prescription(String drug, String time, String days, int daysLeft){
        this.time = new SimpleStringProperty(time);
        this.drug = new SimpleStringProperty(drug);
        this.daysLeft = new SimpleIntegerProperty(daysLeft);
        this.days = new SimpleStringProperty(days);
    }

public final SimpleStringProperty drugProperty(){
        return drug;
}

public final String getDrug(){
        return drug.get();
}

public final void setDrug(String value){
        days.set(value);
}
    public final SimpleStringProperty daysProperty(){
        return days;
    }

    public final String getDays(){
        return days.get();
    }

    public final void setProject(String value){
        days.set(value);
    }
    public final SimpleStringProperty timeProperty(){
        return time;
    }

    public final String getTime(){
        return time.get();
    }

    public final void setTime(String value){
        time.set(value);
    }

    public final SimpleIntegerProperty daysLeftProperty(){
        return daysLeft;
    }

    public final int getDaysLeft(){
        return daysLeft.get();
    }

    public final void setDaysLeft(int value){
        daysLeft.set(value);
    }

}
