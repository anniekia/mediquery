package medi.mediquery;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class QuestionController extends Application{



    public String mayoBase = "https://www.mayoclinic.org/search/search-results?q=";
    public String mayoLink = mayoBase;
    public String mayoSpacer = "%20";

    public String clevelandBase = "https://my.clevelandclinic.org/search?q=";
    public String clevelandLink = clevelandBase;
    public String clevelandSpacer = "%20";

    public String kidsBase = "https://kidshealth.org/content/kidshealth/us/en/searchresults.html?q=";
    public String kidsLink = kidsBase;
    public String kidsSpacer = "%20";

    public String NIHBase = "https://search.nih.gov/search?affiliate=hip&query=";
    public String NIHLink = NIHBase;
    public String NIHSpacer = "+";

    public String pillsBase = "https://www.worstpills.org/site-search?q=";
    public String pillsLink = pillsBase;
    public String pillsSpacer = "+";

    @FXML
    private TextField questionText;

    @FXML
    private Label StatusLabel;
    //converts question into a usable URL for each hyperlink for the events bellow
    @FXML
    void askQuestion(ActionEvent event){
        mayoLink = mayoBase + questionText.getText().replace(" ", mayoSpacer);
        clevelandLink = clevelandBase + questionText.getText().replace(" ", clevelandSpacer);
        kidsLink = kidsBase + questionText.getText().replace(" ", kidsSpacer);
        NIHLink = NIHBase + questionText.getText().replace(" ", NIHSpacer);
        pillsLink = pillsBase + questionText.getText().replace(" ", pillsSpacer);
        StatusLabel.setTextFill(Color.color(0,1,0));
        StatusLabel.setText("Question Processed");
    }

    @FXML
    void openKidsLink(ActionEvent event) {
        getHostServices().showDocument(kidsLink);
    }
    @FXML
    void openClevelandLink(ActionEvent event) {
        getHostServices().showDocument(clevelandLink);
    }

    @FXML
    void openMayoLink(ActionEvent event) {
        getHostServices().showDocument(mayoLink);
    }

    @FXML
    void openNIHLink(ActionEvent event) {
        getHostServices().showDocument(NIHLink);
    }

    @FXML
    void openPillsLink(ActionEvent event) {
        getHostServices().showDocument(pillsLink);
    }

    //ignore this, needed for getHostServices to work for some reason
    @Override
    public void start(Stage stage) throws Exception {

    }
}

