# README #

### What is this repository for? ###

* Prescription tracker app with questions regarding your medication
* Version 1.0!!!

### Patch Notes ###
* UI fully works, all buttons work as expected

### How to Use ###
* Sign-In first, or make an account if you do not have one already. As of right now there is no way to recover a lost password
* Add a new prescription, using the times and days in preferred format. When you are done with the day mark that you've taken your perscriptions, and all will be ticked off
* If you have medical questions, click the question button and type it in, then click the link of hte source you want

### Things to Do ###
* Add a way to recover passwords
* Add notifications
* Make prescriptions auto-refresh after one is added.

Although this project is done, I will continue to improve the application for personal development.

### Project Split ###
Annabelle Roche 40%
Ignacio Antequera Sanchez 30%
Haofan Wang 30%